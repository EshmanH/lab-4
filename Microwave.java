import java.io.ObjectInputStream.GetField;
import java.util.Timer;

public class Microwave{
		
	private int wattage;
	private int height;
	private int width;
	private String brand;
	private int timer;

	public Microwave(int wattage, int height, int width, String brand){
		this.wattage = wattage;
		this.height = height;
		this.width = width;
		this.brand = brand;
		this.timer = 0;
	}

	public void setTimer(int timeInMinutes) {
		if (isValidTimer(timeInMinutes)) {
		  this.timer = timeInMinutes;
		  System.out.println("Timer set to "+timeInMinutes+" minutes.");
		} else {
		  System.out.println("Invalid timer value.");
		}
	  }
	
	private boolean isValidTimer(int timeInMinutes) {
		if (timeInMinutes < 0 || timeInMinutes > 60) {
		  return false;
		}
		return true;
	  }

	public void setWattage(int wattage) {
		this.wattage = wattage;
	}
	public int getWattage() {
		return wattage;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	public int getHeight() {
		return height;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	public int getWidth() {
		return width;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getBrand() {
		return brand;
	}
	
	public int getTimer() {
		return timer;
	}

	public String toString(){
		return "WATTAGE: " + wattage + " HEIGHT: " + height + " WIDTH: " + width + " BRAND: " + brand;
	}

	public String printBrandAndWattage(){

		String print = "Brand name: " + brand + "\n" + "Wattage: " + wattage;
		return print;
	}

	public String willFit(int plateWidth, int plateHeight){
		if (plateHeight < height && plateWidth < width){
			return "Your plate of food will fit inside the microwave.";
		}
		else{
			return "Your plate of food will not fit inside your microwave.";
		}
	}

}
